# ADB WiFi  

AFB WiFi is a simple android utility for enabling ADB over TCP/IP on an android smartphone.  
This requires root.  
  
Through wifi enabled adb, you no longer need a usb connect to debug applications.  
It's as simple as running
> $ adb connect 192.168.1.2  

All code is opensource, use as you like.